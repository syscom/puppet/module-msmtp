# class: msmtp::config
#
# msmtp config file management
class msmtp::config inherits msmtp {
  file { $msmtp::params::configfile:
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('msmtp/msmtp.epp')
  }
}

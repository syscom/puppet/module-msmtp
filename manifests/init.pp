# class: msmtp
#
# Manages msmtp for a host, installs the package and configures the global
# configuration file
class msmtp (
  String  $host,
  String  $from        = $msmtp::params::from,
  Boolean $auto_from   = $msmtp::params::auto_from,
  String  $maildomain  = $msmtp::params::maildomain,
  String  $domain      = $msmtp::params::domain,
  String  $aliases     = $msmtp::params::aliases,
  String  $syslog      = $msmtp::params::syslog

) inherits msmtp::params {

  class { 'msmtp::config': }

  package { $msmtp::params::package :
    ensure => present
  }
}

# class: msmtp::params
#
# Default parameters for msmtp-mta
class msmtp::params {

  $host       = 'smtp.example.org'
  $from       = 'root@example.org'
  $auto_from  = true
  $maildomain = $::domain
  $domain     = $::fqdn

  $aliases = '/etc/aliases'
  $syslog  = 'LOG_MAIL'

  case $::osfamily {
    'Debian': {
      $package                = 'msmtp-mta'
      $configfile             = '/etc/msmtprc'
    }
    'Gentoo': {
      $package                = 'mail-mta/msmtp'
      $configfile             = '/etc/msmtprc'
    }
    default: {
      fail('OS not supported by msmtp')
    }
  }
}
